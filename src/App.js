import React, { Component } from "react";
import Header from "./components/Header";
import Offer from "./components/Offer";
import Trust from "./components/Trust";
import Processes from "./components/Processes";
import Reviews from "./components/Reviews";
import Currency from "./components/Currency";
import Partners from "./components/Partners";
import Footer from "./components/Footer";
import LoginForm from "./components/LoginForm";
import RegForm from "./components/RegForm/regForm";

import "./index.scss";

export default class App extends Component {

  state = {
    loginForm: false,
    regForm: false,
  };

  showModal = (type) => {
    if (type === 'login') {
      this.setState(state => {
        return {
          loginForm: !state.loginForm
        }
      })
    }

    if (type === 'reg') {
      this.setState(state => {
        return {
          regForm: !state.regForm
        }
      })
    }
  };

  regForm = () => {
    this.setState(state => {
      return {
        loginForm: !state.loginForm,
        regForm: !state.regForm
      }
    })
  };

  render() {
    const {loginForm,regForm} = this.state;

    return (
      <div className="mainBlock">
        <Header
          showModal={this.showModal}
        />
        <Offer />
        <Trust />
        <Processes />
        <Reviews />
        <Currency />
        <Partners />
        <Footer />
        {loginForm && (
          <LoginForm
            showModal={this.showModal}
            regForm={this.regForm}
          />
        )}
        {regForm && (
          <RegForm
            showModal={this.showModal}
          />
        )}
      </div>
    );
  }
}
