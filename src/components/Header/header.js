import React from "react";
import { pathUrl } from "../../data";
import HamburgerMenu from "../HamburgerMenu";

import "./index.scss";

const Header = props => {
  const { showModal } = props;

  return (
    <div className="container">
      <div className="header header-wrapper">
        <div className="header-content">
          <div className="header-logo">
            <img src={`${pathUrl.iconsUrl}/logo.svg`} alt="logo" />
          </div>
          <div className="header-menu d-none d-lg-flex">
            <div className="menu">
              <div className="menu-item">Обмен валют</div>
              <div className="menu-item">Как это работает</div>
              <div className="menu-item questions-item">
                Вопросы и ответы
                <div className="common-questions-wrapper d-none d-lg-flex">
                  <div className="common-questions">
                    <div className="question-columns-wrapper">
                      <div className="question-column">
                        <h4>Общие вопросы:</h4>
                        <ul>
                          <li>
                            <a href={"#"}>Как зарегистрировать кошелек</a>
                          </li>
                          <li>
                            <a href={"#"}>Как выглядит адрес кошелька</a>
                          </li>
                          <li>
                            <a href={"#"}>Что такое криптовалюта</a>
                          </li>
                          <li>
                            <a href={"#"}>Что такое блокчейн</a>
                          </li>
                        </ul>
                      </div>
                      <div className="question-column">
                        <h4>Проблемы с обменом</h4>
                        <ul>
                          <li>
                            <a href={"#"}>Не пришли деньги после обмена</a>
                          </li>
                          <li>
                            <a href={"#"}>Долго не приходит смс с кодом</a>
                          </li>
                          <li>
                            <a href={"#"}>Я не правильно указал кошелек</a>
                          </li>
                          <li>
                            <a href={"#"}>Сколько длится обмен?</a>
                          </li>
                        </ul>
                      </div>
                      <div className="question-column">
                        <h4>Продажа криптовалюты</h4>
                        <ul>
                          <li>
                            <a href={"#"}>Какие криптовалюты вы принимаете?</a>
                          </li>
                          <li>
                            <a href={"#"}>Способы вывода денег</a>
                          </li>
                          <li>
                            <a href={"#"}>Какая коммисия на обмен?</a>
                          </li>
                        </ul>
                      </div>
                      <div className="question-column">
                        <h4>Не нашли ответ?</h4>
                        <button className="supports">
                          Написать в тех. поддержку
                        </button>
                      </div>
                    </div>
                    <button className="all-questions">
                      Все вопросы и ответы
                      <img
                        className="question-arrow"
                        src={`${pathUrl.iconsUrl}/arrow.svg`}
                        alt="arrow"
                      />
                    </button>
                  </div>
                </div>
              </div>
              <div className="menu-item">Разработчикам</div>
            </div>
          </div>
          <div className="header-login d-none d-lg-flex">
            <div className="login-icon">
              <img src={`${pathUrl.iconsUrl}/lock.svg`} alt="lock" />
            </div>
            <div className="login-button">
              <button onClick={() => showModal("login")}>Вход</button>
            </div>
            /
            <div className="registration-button">
              <button onClick={() => showModal("reg")}>Регистрация</button>
            </div>
          </div>
          <HamburgerMenu
          showModal={showModal}
          />
        </div>
      </div>
    </div>
  );
};

export default Header;
