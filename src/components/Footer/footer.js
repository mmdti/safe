import React from 'react'
import {pathUrl} from "../../data"

import './index.scss'

const Footer = () => {
  return (
    <footer className="footer">
      <div className="footer-content container">
        <div className="footer-logo-info d-none d-md-block">
          <div>
            <img src={`${pathUrl.iconsUrl}/logo.svg`} alt="logo" />
          </div>
          <div className="phone">
            +371 67 16 60 00
          </div>
          <div className="email">
            support@safecurrency.com
          </div>
        </div>
        <div className="footer-links main">
          <div><a href="#">Помощь</a></div>
          <div><a href="#">Условия использования</a></div>
          <div><a href="#">Политика конфиденциальности</a></div>
          <div><a href="#">Партнерская программа</a></div>
        </div>
        <div className="footer-links additional d-none d-md-block">
          <div><a href="#">Обмен валют</a></div>
          <div><a href="#">Вопросы и ответы</a></div>
          <div><a href="#">Разработчикам</a></div>
          <div><a href="#">Личный кабинет</a></div>
        </div>
        <div className="footer-social">
          <div className="copyright">© 2018-2019 Safe currency</div>
          <div className="social-links">
            <a href="" className="link">
              <img src={`${pathUrl.iconsUrl}/fb.svg`} alt="fb" />
            </a>
            <a href="" className="link">
              <img src={`${pathUrl.iconsUrl}/tw.svg`} alt="fb" />
            </a>
            <a href="" className="link">
              <img src={`${pathUrl.iconsUrl}/in.svg`} alt="fb" />
            </a>
            <a href="" className="link">
              <img src={`${pathUrl.iconsUrl}/vk.svg`} alt="fb" />
            </a>
          </div>
          <div className="company-info-mobile">
            Safe Currency LLP, Company number OC421760, Registered office address: 20-22 Wenlock Road, London, England,
            N1 7GU Phone: +44 20 3287 1368
          </div>
          <div className="payments">
            <div className="payment"><img src={`${pathUrl.iconsUrl}/visa.svg`} alt="visa"/></div>
            <div className="payment"><img src={`${pathUrl.iconsUrl}/mscard.svg`} alt="mastercard"/></div>
            <div className="payment"><img src={`${pathUrl.iconsUrl}/sepa.svg`} alt="sepa"/></div>
          </div>
        </div>
      </div>
      <div className="company-info">
        <div>
          Safe Currency LLP, Company number OC421760, Registered office address: 20-22 Wenlock Road, London, England, N1
          7GU Phone: +44 20 3287 1368
        </div>
      </div>
    </footer>
  )
};

export default Footer