import React, { Component } from "react";
import { pathUrl, courses } from "../../data";
import Slider from "react-slick";

import 'slick-carousel/slick/slick.scss'
import 'slick-carousel/slick/slick-theme.scss'

import "./index.scss";

export default class Offer extends Component {
  state = {
    slideIndex: 0
  };

  handleAfterSlide = slideIndex => {
    this.setState({ slideIndex });
  };


  render() {
    const PrevArrowBtn = ({onClick}) => {
      return <button onClick={onClick} className='slickBtn slickBtnPrev'>
        <img src={`${pathUrl.iconsUrl}/left-arrow.svg`} alt='prevArrow'/>
      </button>
    };

    const NextArrowBtn = ({onClick}) => {
      return <button onClick={onClick} className='slickBtn slickBtnNext'>
        <img src={`${pathUrl.iconsUrl}/right-arrow.svg`} alt='nextArrow'/>
      </button>
    };

    const params = {
      dots: false,
      infinite: true,
      speed: 500,
      slidesToShow: 3,
      slidesToScroll: 1,
      centerPadding: 30,
      prevArrow: <PrevArrowBtn />,
      nextArrow: <NextArrowBtn />,
      responsive: [
        {
          breakpoint: 1200,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 1,
            infinite: false,
          }
        },
        {
          breakpoint: 992,
          settings: {
            slidesToShow: 3,
            slidesToScroll: 1,
            infinite: false,
          }
        },
        {
          breakpoint: 768,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
            infinite: false,
            centerMode: true,
            centerPadding: '60px',
            arrows: false
          }
        }
      ]
    };

    return (
      <div className="container">
        <div className="exchange">
          <div className="exchange-info">
            <h1 className="exchange-header">
              Обмен криптовалют на фиатные деньги
            </h1>
            <div className="exchange-description">
              Купить криптовалюту с любой банковской карты
            </div>
            <div className="currency-info">
              <div className="currency-cards-wrapper">
                <Slider {...params}>
                  {courses.map((item, idx) => (
                    <div className="currency-card" key={idx}>
                      <div className="currency-card-header">
                        <div className="currency-from-symbol">
                          <img src={item.url} alt="bitcoin" />
                        </div>
                        <div className="currency-from-name">{item.cfm}</div>/
                        <div className="currency-to-name">{item.ctn}</div>
                      </div>
                      <div className="currency-to-result">{item.ctr}</div>
                      <div className="currency-difference">{item.cd}</div>
                      <div className="currency-updated-time">
                        За последние 24 часа
                      </div>
                    </div>
                  ))}
                </Slider>
              </div>
            </div>
          </div>
          <div className="exchange-form">Здесь должна быть форма</div>
        </div>
      </div>
    );
  }
}
