import React, {Component} from 'react'

import './index.scss'
import {pathUrl} from "../../data";

export default class HamburgerMenu extends Component {
  state = {
    showHamburger: false
  };

  showHamburgerMenu = () => {
    this.setState(prevState => {
      return {
        showHamburger: !prevState.showHamburger
      }
    })
  };

  render() {
    const {showModal} = this.props;
    const {showHamburger} = this.state;
    let classNames = '';
    const hamburgerBtn = !showHamburger ? <i className="fas fa-bars"/> : <i className="fas fa-times" />;

    if (showHamburger) classNames += 'show';

    return (
      <React.Fragment>
        <button
          className='hamburger__btn d-md-none'
          onClick={this.showHamburgerMenu}
        >
          {hamburgerBtn}
        </button>
        <div className={`modalMenu ${classNames}`}>

        </div>
        <div className={`hamburger ${classNames} d-lg-none`}>
          <button
            className='hamburger__btn d-none d-md-block'
            onClick={this.showHamburgerMenu}
          >
            {hamburgerBtn}
          </button>
          <div className='hamburger__list'>
            <nav className='hamburger__list_item'>
              <a href="#">Обмен валют</a>
              <a href="#">Как это работает</a>
              <a href="#">Вопросы и ответы</a>
              <a href="#">Разработчикам</a>
            </nav>
            <div className="hamburger__login header-login">
              <div className="login-icon">
                <img src={`${pathUrl.iconsUrl}/lock.svg`} alt="lock" />
              </div>
              <div className="login-button">
                <button onClick={() => showModal("login")}>Вход</button>
              </div>
              /
              <div className="registration-button">
                <button onClick={() => showModal("reg")}>Регистрация</button>
              </div>
            </div>
          </div>
        </div>
      </React.Fragment>
    )
  }
}