import React from "react";
import { trustInfo } from "../../data";

import "./index.scss";

const Trust = () => {
  return (
    <div className="container">
      <div className="trust content-block">
        <h2 className="content-block-header">Почему я должен доверять вам?</h2>
        <div className="trust-cards-wrapper">
          {trustInfo.map((item, idx) => (
            <div className='trust-card-block'>
              <div className="trust-card" key={idx}>
                <div className="trust-image-wrapper">
                  <img src={item.url} alt={item.alt} />
                </div>
                <h3 className="trust-card-header">{item.title}</h3>
                <div className="trust-card-description">{item.description}</div>
              </div>
            </div>
          ))}
        </div>
      </div>
    </div>
  );
};

export default Trust;
