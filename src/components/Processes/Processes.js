import React from 'react'
import {pathUrl} from "../../data";

import './index.scss'

const Processes = () => {
  return (
    <div className='processesBlock'>
      <div className="container">
        <div className="process content-block">
          <h3 className="content-block-header">Ок, а как происходит обмен?</h3>
          <div className="steps-wrapper">
            <div className="step">
              <div>
                <h4 className="step-header">
                  Вы выбираете валюту
                  и способ оплаты
                </h4>
                <div className="step-description">
                  Мы принимаем: visa, mastercard, SEPA, qiwi, paypal, яндекс.деньги
                </div>
              </div>
              <div className="step-arrow-horisontal d-none d-md-block">
                <img src={`${pathUrl.iconsUrl}/arrow.svg`} alt="arrow"/>
              </div>
              <div className="step-arrow-vertical d-md-none">
                <img src={`${pathUrl.iconsUrl}/down-arrow.svg`} alt="arrow"/>
              </div>
            </div>
            <div className="step">
              <div>
                <h4 className="step-header">
                  Вы выбираете валюту
                  и способ оплаты
                </h4>
                <div className="step-description">
                  Мы принимаем: visa, mastercard, SEPA, qiwi, paypal, яндекс.деньги
                </div>
              </div>
              <div className="step-arrow-horisontal d-none d-md-block">
                <img src={`${pathUrl.iconsUrl}/arrow.svg`} alt="arrow"/>
              </div>
              <div className="step-arrow-vertical d-md-none ">
                <img src={`${pathUrl.iconsUrl}/down-arrow.svg`} alt="arrow"/>
              </div>
            </div>
            <div className="step">
              <div>
                <h4 className="step-header">
                  Вы выбираете валюту
                  и способ оплаты
                </h4>
                <div className="step-description">
                  Мы принимаем: visa, mastercard, SEPA, qiwi, paypal, яндекс.деньги
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
};

export default Processes