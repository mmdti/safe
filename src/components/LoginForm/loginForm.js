import React, {Component} from 'react'
import {pathUrl} from "../../data"

import './index.scss'

export default class LoginForm extends Component {
  state = {
    className: null
  };

  componentDidMount() {
    this.setState({
      className: 'show'
    })
  }

  render() {
    const {className} = this.state;
    const {showModal, regForm} = this.props;
    return (
      <div className={`popup-wrapper ${className}`}>
        <div className="popup">
          <button
            className="closeBtn"
            onClick={() => {
              showModal('login');
              this.setState({
                className: null
              })
            }}
          >
            <img src={`${pathUrl.iconsUrl}/close.svg`} alt="close"/>
          </button>
          <form>
            <div className="email-filed field">
              <label htmlFor="email">Email</label>
              <input type="email" placeholder="example@mail.com" name="email"/>
            </div>
            <div className="password-filed field">
              <label htmlFor="password">Password</label>
              <input type="password" name="password"/>
              <a href="restore-password.html" className="forgot-password">Забыл</a>
            </div>
            <div>
              <button type="submit" className="submit-button">Войти</button>
            </div>
            <div className="no-account">
              <span>Нет аккаунта?</span>
              <button onClick={regForm}>Зарегистрируйтесь!</button>
            </div>
          </form>
        </div>
      </div>
    )
  }
};
