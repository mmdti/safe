import React, { Component } from "react";
import { pathUrl } from "../../data";

import "./index.scss";

export default class RegForm extends Component {
  state = {
    className: null
  };

  componentDidMount() {
    this.setState({
      className: 'show'
    })
  }

  render() {
    const {className} = this.state;
    const {showModal} = this.props;

    return (
      <div className={`popup-wrapper ${className}`}>
        <div className="popup registration">
          <button
            className="closeBtn"
            onClick={() => showModal('reg')}
          >
            <img src={`${pathUrl.iconsUrl}/close.svg`} alt="close" />
          </button>
          <form>
            <div className="pair-field">
              <div className="email-filed field">
                <label htmlFor="firstname">Имя</label>
                <input type="text" name="firstname" />
              </div>
              <div className="password-filed field">
                <label htmlFor="lastname">Фамилия</label>
                <input type="text" name="lastname" />
              </div>
            </div>
            <div className="pair-field">
              <div className="field">
                <label htmlFor="email">Email</label>
                <input
                  type="email"
                  placeholder="example@mail.com"
                  name="email"
                />
              </div>
              <div className="password-filed field">
                <label htmlFor="adress">Адрес</label>
                <input type="text" name="adress" />
              </div>
            </div>
            <div className="pair-field">
              <div className="field">
                <label htmlFor="password-create">Придумайте пароль</label>
                <input type="password" name="password-create" />
              </div>
              <div className="password-filed field">
                <label htmlFor="password-repeat">Повторите пароль</label>
                <input type="password" name="password-repeat" />
              </div>
            </div>
            <div className="submit-button-wrapper">
              <button type="submit" className="submit-button">
                Зарегистрироваться
              </button>
            </div>
          </form>
        </div>
      </div>
    );
  }
}
