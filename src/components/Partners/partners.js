import React from "react";
import { pathUrl } from "../../data";

import "./index.scss";

const Partners = () => {
  return (
    <div className="container">
      <div className="partners content-block">
        <h3 className="content-block-header">Наши партнеры</h3>
        <div className="partners-wrapper">
          <div className="partners-cards-wrapper">
            <div className="partner-card">
              <img src={`${pathUrl.iconsUrl}/partner1.svg`} alt="partner" />
            </div>
            <div className="partner-card">
              <img src={`${pathUrl.iconsUrl}/partner1.svg`} alt="partner" />
            </div>
            <div className="partner-card">
              <img src={`${pathUrl.iconsUrl}/partner1.svg`} alt="partner" />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Partners;
