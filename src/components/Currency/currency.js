import React from "react";

import "./index.scss";

const Currency = () => {
  return (
    <div className="currencyBlock">
      <div className="container">
        <div className="content-block">
          <h3 className="content-block-header">Пока вы думаете, курс растет</h3>
          <div className="currency-grow">
            Купите криптовалюту сейчас. Не откладывайте на завтра, ведь завтра
            она может подорожать ;)
          </div>
          <div className="form-mock">Здесь должна быть форма</div>
        </div>
      </div>
    </div>
  );
};

export default Currency;
