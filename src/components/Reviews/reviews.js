import React from 'react'
import {pathUrl} from "../../data";

import './index.scss'

const Reviews = () => {
  return (
    <div className='container'>
      <div className="content-block">
        <h3 className="content-block-header">Круто! А другие что говорят?</h3>
        <div className="comments-wrapper">
          <div className="comments-cards-wrapper">
            <div className="comment-card">
              <div className="comment-header">
                <div className="comment-name">Рик Санчез</div>
                <div className="comment-stars"></div>
              </div>
              <div className="comment-text">
                Ваббалабу даб даб! Это самый отличный сервис в этом измерении! Обменял немного шмеклей на биткойны. Все
                очень быстро и просто!
              </div>
              <div className="comment-read-more">
                Читать полностью
              </div>
            </div>
            <div className="comment-card">
              <div className="comment-header">
                <div className="comment-name">Марти Макфлай</div>
                <div className="comment-stars"></div>
              </div>
              <div className="comment-text">
                Вместе с Доком вернулись в это время, чтобы закупиться биткойнами. Выбрали Safe currency потому, что они
                профессионалы своего дела. P.S. Скупайте биткойны. Потом спасибо скажете ;)
              </div>
              <div className="comment-read-more">
                Читать полностью
              </div>
            </div>
          </div>
          <div className="fake-comments-warning">
            Мы понимаем, что в интернете полно фейковых отзывов, но мы размещаем реальные :) Поэтому присылайте свои
            отзывы к нам на почту: inbox@safecurrency.com, или отмечайте нас в соц. сетях @safecurrency
          </div>
          <div className="excellent-mark">
            Safecurrency оценили "Excellent" на <img src={`${pathUrl.iconsUrl}/trustpilot.svg`} alt="trustpilot"/>
          </div>
        </div>
      </div>
    </div>
  )
};

export default Reviews