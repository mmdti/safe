const pathUrl = {
  backgroundUrl: '/img/background',
  iconsUrl: '/img/icons'
};

const trustInfo = [
  {
    url: '/img/trust/PCI.svg',
    alt: 'PCI',
    title: 'Мы соответствуем PCI Security Standards',
    description: 'Это значит что данные вашей карты не попадут к третим лицам.'
  },
  {
    url: '/img/trust/license.svg',
    alt: 'license',
    title: 'Соответствуем всем требованиям законодательства',
    description: 'Сделка проходит легально, а это значит, что вы обязательно получите свои средства. Лицензия'
  },
  {
    url: '/img/trust/coins.svg',
    alt: 'coins',
    title: 'Имеем собственные резервы для быстрого обмена',
    description: 'Вы почти моментально получите средства на свой счет.'
  },
  {
    url: '/img/trust/exchange.svg',
    alt: 'exchange',
    title: 'У нас лучший курс обмена',
    description: 'Мы получаем прямой курс с ведущих криптобирж и делаем вам лучшие условия для обмена'
  }
];

const courses = [
  {
    url: `${pathUrl.iconsUrl}/bitcoin.svg`,
    cfm: 'BTC',
    ctn: 'EUR',
    ctr: '€ 7903.56',
    cd: '+2,7%'
  },
  {
    url: `${pathUrl.iconsUrl}/bitcoin.svg`,
    cfm: 'BTC',
    ctn: 'EUR',
    ctr: '€ 7903.56',
    cd: '+2,7%'
  },
  {
    url: `${pathUrl.iconsUrl}/bitcoin.svg`,
    cfm: 'BTC',
    ctn: 'EUR',
    ctr: '€ 7903.56',
    cd: '+2,7%'
  },
  {
    url: `${pathUrl.iconsUrl}/bitcoin.svg`,
    cfm: 'BTC',
    ctn: 'EUR',
    ctr: '€ 7903.56',
    cd: '+2,7%'
  }
];

export {
  pathUrl,
  trustInfo,
  courses
}